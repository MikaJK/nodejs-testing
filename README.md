# nodejs-testing

This repository contains examples of Sinon test.

Objective is to stub synchronous and asynchronous dependencies. Stub are used, becasue the functions having stubs are not asserted.

Asynchronous methods are implemented with timeout and with promises.

All implementation is done according to ES6, and therefore numerous Babel packages are required to be installed and proper Babel configuration must be in place. See package.json and .babel files for more details.

## Test Framework

The used test framework is Jest. All tests should work equally with Mocha and Chai, but some changes are then needed for test dependencies and assertions.

## Stub Setup

The Sinon stubs are implemented with Sinon sandbox, which makes the restoring the stubs much easier.

Sandbox is created and restored in beforeEach and afterEach functions, which reduces the amout of code in the actual tests.

## Luey

The luey.js has dependency towards nephew.js, which implementes three functions.

The stubs for the nephew.js are created for the functions of imported nephew instance:

    sandbox.stub(nephew, 'getFirstName').returns(stubFirstName);
    sandbox.stub(nephew, 'getLastName').returns(stubLastName);
    sandbox.stub(nephew, 'getFullName').resolved(stubFullName);

## Donald

The donald.js has a dependency towards uncle.js, which implements a class with three functions.

The stubs of the uncle.js are created for the functions of prototype of imported Uncle class:

    sandbox.stub(Uncle.prototype, 'getFirstName').returns(stubFirstName);
    sandbox.stub(Uncle.prototype, 'getLastName').returns(stubLastName);
    sandbox.stub(Uncle.prototype, 'getFullName').resolves(stubFirstName);


