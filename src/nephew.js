// synchronous.
export function getFirstName(firstName) {
    return firstName;
}

// asynchronous.
export async function getLastName(lastName) {
    
    setTimeout(function () {
        console.log('Waiting for 1 second');
    }, 1000);

    return lastName;
}

// promise.
export async function getFullName(firstName, lastName) {
    return Promise.resolve(`${firstName} ${lastName}`);
}