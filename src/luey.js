import { getFirstName, getLastName, getFullName } from './nephew';

export function getLueysFirstName() {
    var firstName = getFirstName('Luey');
    return firstName;
}

export async function getLueysLastName() {
    var lastName = await getLastName('Duck');
    return lastName;
}

export async function getLueysFullName() {
    try {
        const fullName = await getFullName('Luey', 'Duck');
        return fullName;
    } catch(error) {
        console.log(error);
    }
}