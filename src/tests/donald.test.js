const sinon = require("sinon");
const { getDonaldsFirstName, getDonaldsLastName, getDonaldsFullName } = require('../donald');
const { Uncle } = require('../uncle');

var sandbox;
var firstName = 'Donald';
var lastName = 'Duck';
var fullName = 'Donald Duck';

describe('donalad', () => {
    
    beforeEach(function () {
        sandbox = sinon.createSandbox();
    });

    afterEach(function () {
        sandbox.restore();
    });

    it('should get real first name', () => {
        expect(getDonaldsFirstName()).toBe(firstName);
    });

    it('should get real last name', async () => {
        expect(await getDonaldsLastName()).toBe(lastName);
    });

    it('should get real full name', async () => {
        expect(await getDonaldsFullName()).toBe(fullName);
    });

    it('should get stub first name', () => {
        var stubFirstName = 'Aku';
        sandbox.stub(Uncle.prototype, 'getFirstName').returns(stubFirstName);
        expect(getDonaldsFirstName()).toBe(stubFirstName);
    });

    it('should get stub last name', async () => {
        var stubLastName = 'Ankka';
        sandbox.stub(Uncle.prototype, 'getLastName').returns(stubLastName);
        expect(await getDonaldsLastName()).toBe(stubLastName);
    });

    it('should get stub full name', async () => {
        var stubFullName = 'Aku Ankka';
        sandbox.stub(Uncle.prototype, 'getFullName').resolves(stubFullName);
        expect(await getDonaldsFullName()).toBe(stubFullName);
    });
});