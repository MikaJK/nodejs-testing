const sinon = require("sinon");
const nephew = require('../nephew');
import { getLueysFirstName, getLueysLastName, getLueysFullName } from '../luey';

var sandbox;
var firstName = 'Luey';
var lastName = 'Duck';
var fullName = 'Luey Duck';

describe("duey", () => {
    
    beforeEach(function () {
        sandbox = sinon.createSandbox();
    });

    afterEach(function () {
        sandbox.restore();
    });

    it('should get real first name', () => {
        expect(getLueysFirstName()).toBe(firstName);
    });

    it('should get real last name', async () => {
        expect(await getLueysLastName()).toBe(lastName);
    });

    it('should get real full name', async () => {
        expect(await getLueysFullName()).toBe(fullName);
    });

    it("should get stub first name", function() {
        var stubFirstName = 'Lupu'
        sandbox.stub(nephew, 'getFirstName').returns(stubFirstName);
        expect(getLueysFirstName()).toBe(stubFirstName);
    });

    it("should get stub last name", async function() {
        var stubLastName = 'Ankka'
        sandbox.stub(nephew, 'getLastName').returns(stubLastName);
        expect(await getLueysLastName()).toBe(stubLastName);
    });

    it("should get stub full name", async function() {
        var stubFullName = 'Lupu Ankka'
        sandbox.stub(nephew, 'getFullName').resolves(stubFullName);
        expect(await getLueysFullName()).toBe(stubFullName);
    });
});