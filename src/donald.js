import { Uncle } from './uncle';

export function getDonaldsFirstName() {
    var donald = new Uncle('Donald', 'Duck');
    var firstName = donald.getFirstName();
    return firstName;
}

export async function getDonaldsLastName() {
    var donald = new Uncle('Donald', 'Duck');
    var lastName = await donald.getLastName();
    return lastName;
}

export async function getDonaldsFullName() {
    var donald = new Uncle('Donald', 'Duck');
    
    try {
        const fullName = await donald.getFullName();
        return fullName;
    } catch(error) {
        console.log(error);
    }
}