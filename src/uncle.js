export class Uncle {
    constructor(firstName, lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    // synchronous.
    getFirstName() {
        return this.firstName;
    }

    // asynchronous.
    async getLastName() {
        return this.lastName;
    }

    // promise.
    getFullName() {
        return Promise.resolve(`${this.firstName} ${this.lastName}`);
    }
}